package cn.lankton.anyshape;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.util.Optional;

/**
 * Created by taofangxin on 16/3/21.
 */
public class AnyshapeImageView extends Image implements Component.DrawTask {
    private Color backColor;

    /**
     * AnyshapeImageView
     *
     * @param context Context
     */
    public AnyshapeImageView(Context context) {
        super(context);
        addDrawTask(this);
    }

    /**
     * AnyshapeImageView
     *
     * @param context Context
     * @param attrSet AttrSet
     */
    public AnyshapeImageView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        addDrawTask(this);
    }

    /**
     * AnyshapeImageView
     *
     * @param context   Context
     * @param attrSet   AttrSet
     * @param styleName String
     */
    public AnyshapeImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        addDrawTask(this);
        parseAttrs(attrSet);
    }

    private void parseAttrs(AttrSet attrSet) {
        backColor = attrSet.getAttr(Constants.AnyshapeBackColor).isPresent() ?
                attrSet.getAttr(Constants.AnyshapeBackColor).get().getColorValue() : Color.TRANSPARENT;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        LogUtil.debug("OnDraw", "Draw...");
    }

    /**
     * decodeResource
     *
     * @param context Context
     * @param id      integer
     * @return pixelmap
     */
    public static PixelMap decodeResource(Context context, int id) {
        try {
            String path = context.getResourceManager().getMediaPath(id);
            if (path.isEmpty()) {
                return null;
            }
            RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            options.formatHint = Constants.Image_Format;
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            decodingOptions.desiredSize = new Size(Constants.NUM_SIX_HUNDRED, Constants.NUM_FOUR_HUNDRED);
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions)).get();
        } catch (Exception e) {
            LogUtil.debug(Constants.TAG_VAL, "" + e.getMessage());
        }
        return null;
    }
}
