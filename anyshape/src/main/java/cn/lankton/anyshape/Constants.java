/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.lankton.anyshape;

import java.util.Locale;

/**
 * Constants
 */
public class Constants {
    /**
     * BUNDLE_NAME
     */
    public static final String BUNDLE_NAME = "cn.lankton.anyshape";

    /**
     * TAG_VAL
     */
    public static final String TAG_VAL = "IOEXCEPTION";

    /**
     * LIB
     */
    public static final String LIB = "lib";

    /**
     * LIBS
     */
    public static final String LIBS = "libs";

    /**
     * AnyshapeBackColor
     */
    public static final String AnyshapeBackColor = "anyshapeBackColor";

    /**
     * Image_Format
     */
    public static final String Image_Format = "image/png";

    /**
     * BUFFER_OFFSET_ID_CONSTANT
     */
    public static final int BUFFER_OFFSET_ID_CONSTANT = 0x4;
    /**
     * Invalid ID
     */
    public static final int INVALID = -1;
    /**
     * SHNUM_ID_EIF32_CONSTANT
     */
    public static final int SHNUM_ID_EIF32_CONSTANT = 0x30;

    /**
     * SHSTRNDX_ID_EIF32_CONSTANT
     */
    public static final int SHSTRNDX_ID_EIF32_CONSTANT = 0x32;

    /**
     * PHOFF_ID_EIF64_CONSTANT
     */
    public static final int PHOFF_ID_EIF64_CONSTANT = 0x20;

    /**
     * TYPE_ID_EIF64_CONSTANT
     */
    public static final int TYPE_ID_EIF64_CONSTANT = 0x10;

    /**
     * SHOFF_ID_EIF64_CONSTANT
     */
    public static final int SHOFF_ID_EIF64_CONSTANT = 0x28;

    /**
     * PHENTSIZE_ID_EIF64_CONSTANT
     */
    public static final int PHENTSIZE_ID_EIF64_CONSTANT = 0x36;

    /**
     * PHNUM_ID_EIF64_CONSTANT
     */
    public static final int PHNUM_ID_EIF64_CONSTANT = 0x38;

    /**
     * SHENTSIZE_ID_EIF64_CONSTANT
     */
    public static final int SHENTSIZE_ID_EIF64_CONSTANT = 0x3A;

    /**
     * SHNUM_ID_EIF64_CONSTANT
     */
    public static final int SHNUM_ID_EIF64_CONSTANT = 0x3C;

    /**
     * SHSTRNDX_ID_EIF64_CONSTANT
     */
    public static final int SHSTRNDX_ID_EIF64_CONSTANT = 0x3E;

    /**
     * ELF_PARSER_MAGIC
     */
    public static final int ELF_PARSER_MAGIC = 0x464C457F;

    /**
     * UNMAP_MAGIC_NUM_CONST
     */
    public static final int UNMAP_MAGIC_NUM_CONST = 3;

    /**
     * COPY_NUM_CONST
     */
    public static final int COPY_NUM_CONST = 0;

    /**
     * READ_NUM_CONST
     */
    public static final int READ_NUM_CONST = -1;

    /**
     * NUM_CONST
     */
    public static final int NUM_CONST = 8;

    /**
     * READ_BUFFER_ELF
     */
    public static final long READ_BUFFER_ELF = 0x4;

    /**
     * READ_BIGENDIAN
     */
    public static final long READ_BIGENDIAN = 0x5;

    /**
     * READ_NUM_PROGRAM
     */
    public static final long READ_NUM_PROGRAM = 0xFFFF;

    /**
     * READ_NUM_RETURN
     */
    public static final long READ_NUM_RETURN = 0xFFFFFFFFL;

    /**
     * BASE_OFFSET_VAL
     */
    public static final long BASE_OFFSET_VAL = 0x8;

    /**
     * VADDR_VAL
     */
    public static final long VADDR_VAL = 0x10;

    /**
     * MEMSZ_VAL
     */
    public static final long MEMSZ_VAL = 0x10;

    /**
     * INFO_VAL
     */
    public static final long INFO_VAL = 0x1C;

    /**
     * OFFSET_VAL_32
     */
    public static final long OFFSET_VAL_32 = 0x4;

    /**
     * VADDR_VAL_32
     */
    public static final long VADDR_VAL_32 = 0x8;

    /**
     * MEMSZ_VAL_32
     */
    public static final long MEMSZ_VAL_32 = 0x14;

    /**
     * EIL_VAL
     */
    public static final int EIL_VAL = 4;

    /**
     * LOCALE
     */
    public static final Locale LOCALE = Locale.US;

    /**
     * EIL_RETURN_VAL
     */
    public static final int EIL_RETURN_VAL = 0xFF;

    /**
     * READ_HALF_VAL
     */
    public static final int READ_HALF_VAL = 2;

    /**
     * GET_SHOT_RETURN_VAL
     */
    public static final int GET_SHOT_RETURN_VAL = 0xFFFF;
    /**
     * NUM_SIX_HUNDRED
     */
    public static final int NUM_SIX_HUNDRED = 600;
    /**
     * NUM_FOUR_HUNDRED
     */
    public static final int NUM_FOUR_HUNDRED = 400;

    private Constants() {
        /* Do nothing */
    }
}
