# ohos_anyshape
  ohos_anyshape: An openharmony library that can help developers to display mask image in any shape.
 
## ohos_anyshape includes:
*this library solution can help developers display pictures in any shape.

## Usage Instructions
1. A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the sample/ folder.

2. The following core classes are the essential interface to ohos_anyshape:
AnyShapeImageView : this class basically we are creating the custom xml layout for any shape image.

3. The steps to initialize the ohos_anyshape and the core classes:
  a. xml file
  
     <cn.lankton.anyshape.AnyshapeImageView
                ohos:id="$+id:iv_text"
                ohos:width="match_content"
                ohos:height="200vp"
                ohos:scale_mode="center"
                ohos:layout_alignment="center"
                ohos:top_margin="-15vp"
                />
   b.Java file 

        PixelMap original = AnyshapeImageView.decodeResource(this, ResourceTable.Media_scene);
        PixelMap mask = AnyshapeImageView.decodeResource(this, ResourceTable.Media_text);
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(mask.getImageInfo().size.width, mask.getImageInfo().size.height);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        PixelMap result = PixelMap.create(initializationOptions);
        Texture texture = new Texture(result);
        Canvas mCanvas = new Canvas(texture);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setBlendMode(BlendMode.DST_IN);
        mCanvas.drawPixelMapHolder(new PixelMapHolder(original), 0, 0, new Paint());
        mCanvas.drawPixelMapHolder(new PixelMapHolder(mask), 0, 0, paint);
        iv_text_image.setPixelMap(texture.getPixelMap());   
				
				
  

 ## Installation instruction

1.For using  module in sample app,include the below library dependency to generate hap/anyshape.har:
Add the dependencies in entry/build.gradle as below :

    dependencies {
      implementation project(path: ':anyshape')
      }

2. Using the anyshape har, make sure to add anyshape.har file in the entry/libs folder and add the below dependency
in build.gradle.
Modify the dependencies in the entry/build.gradle file.

  dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
 }



