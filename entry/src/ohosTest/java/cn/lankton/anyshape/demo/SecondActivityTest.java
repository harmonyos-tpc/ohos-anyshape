package cn.lankton.anyshape.demo;

import junit.framework.TestCase;

import java.security.SecureRandom;
import java.util.Random;

public class SecondActivityTest extends TestCase {

    public void testGetRandom() {
        int val = 4;
        int rnd = new SecureRandom().nextInt(5);
        if (val == rnd) {
            assertEquals(val, rnd);
        } else {
            assertNotSame(val, rnd);
        }
    }
}