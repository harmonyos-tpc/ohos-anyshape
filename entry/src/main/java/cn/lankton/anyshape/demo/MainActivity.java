
package cn.lankton.anyshape.demo;

import cn.lankton.anyshape.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;

/**
 * MainAbility
 */
public class MainActivity extends Ability {
    private Button jump;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initialValues();
    }

    private void initialValues() {
        jump = (Button) findComponentById(ResourceTable.Id_jump);
        jump.setClickedListener(component -> {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withBundleName(getBundleName())
                    .withAbilityName(SecondActivity.class.getName())
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
        });
    }
}
