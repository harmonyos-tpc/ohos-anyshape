package cn.lankton.anyshape.demo;

import cn.lankton.anyshape.AnyshapeImageView;
import cn.lankton.anyshape.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.render.*;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.security.SecureRandom;

/**
 * SecondAbility
 */
public class SecondActivity extends Ability {
    private Button btn_change;
    private DependentLayout root;
    private AnyshapeImageView iv_ring_image;
    private AnyshapeImageView iv_text_image;
    private AnyshapeImageView iv_panda_image;
    public int[] colors = {ResourceTable.Media_imag_1, ResourceTable.Media_imag_2,
            ResourceTable.Media_imag_3, ResourceTable.Media_imag_4, ResourceTable.Media_imag_5};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_second_ability);
        initializes();
    }

    private void initializes() {
        btn_change = (Button) findComponentById(ResourceTable.Id_btn_change);
        iv_panda_image = (AnyshapeImageView) findComponentById(ResourceTable.Id_iv_panda);
        iv_text_image = (AnyshapeImageView) findComponentById(ResourceTable.Id_iv_text);
        iv_ring_image = (AnyshapeImageView) findComponentById(ResourceTable.Id_iv_ring);
        maskElementPanda();
        maskElementText();
        maskColor();
        btn_change.setClickedListener(component -> {
            maskColor();
        });
    }

    private void maskElementText() {
        PixelMap original = AnyshapeImageView.decodeResource(this, ResourceTable.Media_scene);
        System.out.println("Values"+original.getImageInfo().size.width);
        System.out.println("Values"+original.getImageInfo().size.height);
        PixelMap mask = AnyshapeImageView.decodeResource(this, ResourceTable.Media_text);
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(mask.getImageInfo().size.width, mask.getImageInfo().size.height);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        PixelMap result = PixelMap.create(initializationOptions);
        Texture texture = new Texture(result);
        Canvas mCanvas = new Canvas(texture);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setBlendMode(BlendMode.DST_IN);
        mCanvas.drawPixelMapHolder(new PixelMapHolder(original), 0, 0, new Paint());
        mCanvas.drawPixelMapHolder(new PixelMapHolder(mask), 0, 0, paint);
        iv_text_image.setPixelMap(texture.getPixelMap());
    }

    private void maskElementPanda() {
        PixelMap original = AnyshapeImageView.decodeResource(this, ResourceTable.Media_kumamon);
        PixelMap mask = AnyshapeImageView.decodeResource(this, ResourceTable.Media_singlestar);
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(mask.getImageInfo().size.width, mask.getImageInfo().size.height);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        PixelMap result = PixelMap.create(initializationOptions);
        Texture texture = new Texture(result);
        Canvas mCanvas = new Canvas(texture);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setBlendMode(BlendMode.DST_IN);
        mCanvas.drawPixelMapHolder(new PixelMapHolder(original), 0, 0, new Paint());
        mCanvas.drawPixelMapHolder(new PixelMapHolder(mask), 0, 0, paint);
        iv_panda_image.setPixelMap(texture.getPixelMap());
    }

    private void maskColor() {
        int color = getRandom(colors);
        PixelMap original = AnyshapeImageView.decodeResource(this, color);
        PixelMap mask = AnyshapeImageView.decodeResource(this, ResourceTable.Media_rings);
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(mask.getImageInfo().size.width, mask.getImageInfo().size.height);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        PixelMap result = PixelMap.create(initializationOptions);
        Texture texture = new Texture(result);
        Canvas mCanvas = new Canvas(texture);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setBlendMode(BlendMode.DST_IN);
        mCanvas.drawPixelMapHolder(new PixelMapHolder(original), 0, 0, new Paint());
        mCanvas.drawPixelMapHolder(new PixelMapHolder(mask), 0, 0, paint);
        iv_ring_image.setPixelMap(texture.getPixelMap());
    }

    /**
     * getRandom
     * @param array intarray
     * @return int
     */
    public static int getRandom(int[] array) {
        int rnd = new SecureRandom().nextInt(array.length);
        return array[rnd];
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
